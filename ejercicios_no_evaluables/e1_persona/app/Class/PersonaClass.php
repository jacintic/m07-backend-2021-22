<?php
class Persona
{
  // attributes
  private $_name;
  private $_surname;
  private $_address;
  private $_comments;
  private $_picture; // path

  /*
  * Setters. Para añadir y modificar valores
  */
  public function setName($_name){
    $this->_name =$_name;
  }
  public function setSurname($_surname){
    $this->_surname =$_surname;
  }
  public function setAddress($_address){
    $this->_address =$_address;
  }
  public function setComments($_comments){
    $this->_comments =$_comments;
  }
  public function setPicture($_picture){
    $this->_picture =$_picture;
  }


    /*
  * Getters. Lo que quiere decir que los atributos de la clase son private
  */
    public function getName(){
      return $this->_name;
    }
    public function getSurname(){
      return $this->_surname;
    }
    public function getAddress(){
      return $this->_address;
    }
    public function getComments(){
      return $this->_comments;
    }
    public function getPicture(){
      return $this->_picture;
    }
}
?>