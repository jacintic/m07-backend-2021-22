<?php
///////////////
// CONSTANTS //
///////////////
// path to the upload folder
define("PATH", "./upload/");
// filetypes allowed
define("FILE_ALLOW_LIST", array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png"));
// max allowed filesize (in MB)
define("MAXSIZE", 3 * 1024 * 1024);
////////////////
// Exceptions //
class UploadError extends Exception
{
}
///////////////////////
// Upload Class propper
class Upload
{
  // attributes
  private $_picture;
  /*
  * Constructor: Inicia la subida del archivo
  * Entrada:
  *   $name: Nombre del elemento del formulario del que gestionamos el $_FILE
  */
  function __construct($name)
  {
    if (isset($name)) {
      $this->_picture = $name;
      $this->upload();
    }
  }

  /*
  * upload: Función que hace las operaciones necesarias para subir el archivo
  * al servidor
  */
  public function upload()
  {
    try {
      // Check if the form was submitted
      if (!$_SERVER["REQUEST_METHOD"] == "POST") {
        // Check if file was uploaded without errors
        throw new UploadError('POST method not detected.');
        if (!isset($_FILES["picture"]) && !$_FILES["picture"]["error"] == 0) {
          throw new UploadError('Errors while uploading.');
        }
      }

      $filename = $this->_picture["name"];

      // check for file within admited formats (image)
      $ext = pathinfo($filename, PATHINFO_EXTENSION);
      if (!array_key_exists($ext, FILE_ALLOW_LIST)) {
        throw new UploadError('Unsuported file type. Only image files allowed.');
      }

      // check for file size within limits  
      if ($this->_picture['size'] > MAXSIZE) {
        throw new UploadError('File size over max file size.' . (MAXSIZE / (1024 * 1024)) . 'MB = Max allowed file size.');
      }

      // check for filename duplicates
      if (file_exists(PATH . $filename)) {
        throw new UploadError('File or file with same filename already exists.');
      }

      // /// ///// FINALLY UPLOAD the image
      move_uploaded_file($this->_picture["tmp_name"], PATH . $filename);

      // redirect on success
        // No procede
          // header('Location: index.php?Succ=true');

      move_uploaded_file($this->_picture["tmp_name"], PATH . $filename);

      //// CATCH STANCES WITH REDIRECT ///
    } catch (UploadError $e) {
      header('Location: index.php?Err=' . $e->getMessage());
      header('Location: index.php?Err=' . $e->getMessage());
    } catch (Exception $e) {
      // aqui iria el redirect al desto con parametro GET
      
      header('Location: index.php?Err=' . $e->getMessage());
    }
  }

  /*
  * Getters. Lo que quiere decir que los atributos de la clase son private
  */
  public function getPath()
  {
    return PATH . $this->_picture['name'];
  }

  /*
  * OPCIONAL:
  * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
  * en la subida de archivos. Por ejemplo:
  * throw new UploadError("Error: Please select a valid file format.");
  */
}
