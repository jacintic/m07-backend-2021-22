<?php
//
include('Class/UploadClass.php');
include('Class/PersonaClass.php');

$upload = new Upload($_FILES["picture"]);
//Imprimimos la ruta de la imagen subida

$persona = new Persona();
// set attributes for Persona
$persona->setName($_POST['name']);
$persona->setSurname($_POST['surname']);
$persona->setAddress($_POST['address']);
$persona->setComments($_POST['comments']);
$persona->setPicture($upload->getPath());
// this will be the title of the homepage
$pg_title = 'Welcome to the profile of ' . $persona->getName() . ' ' . $persona->getSurname();
// calling to header with title, bootstrap css and header(bootstrap div)
include 'Includes/_header.php';


// Print persona template

?>
<div class="row justify-content-center">
    <div class="col-3 border rounded" style="overflow:hidden;">
        <div class="row mt-0 justify-content-center bg-dark" style="max-height: 50vh;">
            <img src='<?php echo $upload->getPath() ?>' style="max-height:50vh;max-width:calc( 100% + 25px);width: auto;">
        </div>
        <div class="row" style="margin-top:calc( 1rem + 8px);">
            <h5>
                <p><span class="fw-bold"><?php echo $persona->getName() . ' ' . $persona->getSurname() ?></p>
            </h5>
        </div>
        <div class="row mb-0">
                <p class="mb-0 fw-bold">Address: <span>
                    </span></p>
            <div class="row mt-0">
                <p>
                    <?php echo $persona->getAddress() ?>
                </p>
            </div>

        </div>
        <div class="row bg-light border">
            <p class="fw-bold">Comments: </p>
            <p>
                <?php echo $persona->getComments() ?>
            </p>
        </div>
    </div>
</div>
<?php
include 'Includes/_footer.php';
?>