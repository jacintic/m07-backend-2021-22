    <?php
    // this will be the title of the homepage
    $pg_title = 'Welcome to the Person Homepage!';
    // calling to header with title, bootstrap css and header(bootstrap div)
    include './Includes/_header.php';
    ?>
    <div class="row justify-content-center">
        <div class="col-10 border rounded">
            <?php
            // check and display error messages
            if (isset($_GET['Err'])) {
            ?>
                <div class="alert alert-danger mt-3" role="alert">
                    <?php
                        echo $_GET['Err'];
                    ?>
                </div>
            <?php
            }
            // check and display upload successful
            if (isset($_GET['Succ'])) {
                ?>
                    <div class="alert alert-success mt-3" role="alert">
                        <?php
                            echo 'File upladed successfully!';
                        ?>
                    </div>
                <?php
                }
            ?>
            <!-- Title of the Form -->
            <h5>Upload Person</h3>
                <form action="ficha_alumno_view.php" method="POST" enctype="multipart/form-data">
                    <!-- Name -->
                    <div class="mb-3">
                        <label for="name" class="form-label">Name:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <!-- Surname -->
                    <div class="mb-3">
                        <label for="surname" class="form-label">Surname:</label>
                        <input type="text" class="form-control" id="surname" name="surname">
                    </div>
                    <!-- Address -->
                    <div class="mb-3">
                        <label for="address" class="form-label">Address:</label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                    <!-- Comments -->
                    <div class="mb-3">
                        <label for="comments" class="form-label">Comments:</label>
                        <input type="text" class="form-control" id="name" name="comments">
                    </div>
                    <!-- Picture -->
                    <div class="mb-3">
                        <label for="picture" class="form-label">Picture:</label>
                        <input type="file" class="form-control" id="name" name="picture">
                    </div>
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary">Send</button>
                    <a href=""></a>
                        <button type="button" class="btn btn-secondary">View gallery</button>
                </form>
        </div>
    </div>
    <?php
    include './Includes/_footer.php';
    ?>