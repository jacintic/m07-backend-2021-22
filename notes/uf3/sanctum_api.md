# Sanctum  
  
(vid)[https://www.youtube.com/watch?v=MT-GJQIY3EU]  
  
## First make migration and installation (partially done):  
  
```
composer require laravel/sanctum

php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"

php artisan migrate
```
  
  
## Add authenticable property to Model  
  
```
use Laravel\Sanctum\HasApiTokens;
 
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
}
```
  

## Add this to App/Http/Kernel.php (replace the api bit)  

```
'api' => [
    \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
    'throttle:api',
    \Illuminate\Routing\Middleware\SubstituteBindings::class,
],
```  
## Test auth (groups)  
  
```
Route::group(['middleware' => ['auth:sanctum]], function () {
    Route::get('/products/search/{name}', [ProductController::class, 'search]);
});

```
  
### Further (postman request setup)  
  
* Headers => Accept => application/json (will return)=> unauthenticated

#### Creating the AuthController  
  
Register method is public, will return the token whe successful. From there you can use the private functions.


