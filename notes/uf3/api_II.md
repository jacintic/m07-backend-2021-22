## API Status codes   
## Most developers are familiar with the most common status codes:  
    
* 200 OK
* 400 Bad Request
* 500 Internal Server Error
  
By starting with these three, you can cover most of the functionalities of your REST API.  
  
Other commonly seen codes include:  
  
* 201 Created
* 204 No Content
* 401 Unauthorized
* 403 Forbidden
* 404 Not Found
  
(more codes)[https://restfulapi.net/http-status-codes/]  
  
  
## Requirements for POSTMAN/sanctum  
  
```
# public
None

# private (protected by sanctum middleware)
* Header => Accept : application/json
* Authorization => Bearer token => token
```
  

