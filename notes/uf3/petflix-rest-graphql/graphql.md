# Petflix graphQL  
  
## Auth, API key.  
  
(sacntum)[https://laravel.com/docs/8.x/sanctum]  
  
**When the mobile application uses the token to make an API request to your application, it should pass the token in the Authorization header as a Bearer token.**  
  
That means the key should be included in the Headers as key: `Authorization`, and value `Bearer keyHere`.  
  
(sanctum video tutorial)[https://www.youtube.com/watch?v=n-J3zw4OWmI]  
  
Exercise, ignore Front End, focus on API testing only.  
  
(sanctum-lighthous)[https://bestofphp.com/repo/daniel-de-wit-lighthouse-sanctum-php-laravel-utilities]  
  

  

