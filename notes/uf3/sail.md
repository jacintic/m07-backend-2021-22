# How to fix sail issue  
  
```
1. delete vendor
2. delete composer.lock (just in case)
3. change the following in composer.json:
. .
"require": {
        "php": "^8.1", <=== to this verssion
. . 
},
    "require-dev": {
        "fakerphp/faker": "^1.9.1",
        "laravel/sail": "^1.13.3", <==== to this verssion (latest)
4. docker prune the following:
. .
docker system prune -a
docker network prune
docker volume prune

5. get the last docker "download" command from Laravel's site using php 8.1
. . here:
docker run --rm     -u "$(id -u):$(id -g)"     -v $(pwd):/var/www/html     -w /var/www/html     laravelsail/php81-composer:latest     composer install --ignore-platform-reqs

```
**NOTE:** Watch for phpmyadmin included in the project

