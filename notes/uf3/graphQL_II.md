# GraphQL (II)  
  
## Install  
 
   
### WARNING  
  
* Downgrade PHP to 8.0 in composer json.  
* install all GraphQL packages
* now you can run migrations  

```
composer require nuwave/lighthouse
  
php artisan vendor:publish --tag=lighthouse-schema
```
  
  
## Auth  
  
Laravel generates the token.  
Sanctum handles the token inside Laravel.  
  
### Detials  
  
\Laravel\Sanctum\Http(...)  
^ this means it is located in /vendor  
  
  
## GraphQL Postman (equivalent)  
  
`composer require mll-lab/laravel-graphql-playground`  
URL: `http://localhost/graphql-playground`   
    
    
## GraphQL Hello World  
  
(hello world with GraphQL)[https://lighthouse-php.com/5/the-basics/fields.html#hello-world]  
  
```
## in sail
php artisan lighthouse:query Hello

## in one of the GraphQL models
type Query {
  hello: String!
}

## inside the Query (App/GraphQL/Queries
 public function __invoke(): string
    {
        return 'world!';
    }

## Query:
{
  hello 
}

```
  
  
## GraphQL Hello World mutation  
  
(link)[https://lighthouse-php.com/5/the-basics/fields.html#fields-with-arguments]  
```
./vendor/bin/sail php artisan lighthouse:mutation Test


## model
test: String!
testII(name: String!): String!

## method
return "Hello, {$args['name']}!";

## query
return "Hello, {$args['name']}!";

```
  
## GraphQL sanctum (setup)  
  
```
./vendor/bin/sail php artisan vendor:publish --tag=lighthouse-config
```
  

  
## Create a query function  
  
```
./vendor/bin/sail php artisan lighthouse:query Test
```
 

## Create a mutation function  
  
```
 ./vendor/bin/sail php artisan lighthouse:mutation argArt
```

## Sanctum Abilities  
  
(link)[https://lighthouse-php.com/master/security/authentication.html#guard-selected-fields]  
    
  
# API III  
  
$request->user();  
  

## Example mutation with custom args  
  
```
mutation myRegister
  {
    argArt(
      name: "John", 
      email: "johhn@gmail.com"
    ){
      myname
      mytoken
    }
    
  }
```

## Example query movies  
  
```

{
	movies {
	  id
	  title
	  generes: genere {
	    name
	  }
	  director {
	    name: dname
	  }
	  actors: actor {
	    name: aname
	  }
	} 
  }
```
  
  
## Register  
  
```
mutation myRegister
  {
    register(
      name: "Paco", 
      email: "pakirrin@gmail.com"
      password: "12345"
    ){
      email
      token
    }
    
  }

```
  
  
## Logout  

```
# request
mutation logout {
  logout(
      email: "paul@gmail.com"
    	token: "1|qn14PUHsg6oedv1pjhozBxurV3qGVqA1aEMdY1K0"
  ) {
    message
  }
}

## req header
{	
    "Authorization": "Bearer 1|qn14PUHsg6oedv1pjhozBxurV3qGVqA1aEMdY1K0"
}

```
  
  
## Get admin token  
  
```
mutation myAdminLogin
  {
    adminGetToken( 
      email: "gumersindo@gmail.com"
      password: "12345"
    ){
      email
      token
    }
  }
```
  
  
## Admin test (auth with admin role)  
  
```
mutation myAdminLogin
  {
    adminGetToken( 
      email: "gumersindo@gmail.com"
      password: "12345"
    ){
      email
      token
    }
  }
```

