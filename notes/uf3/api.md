# APIs (Aplication Programming Interface)  
  
Between the aplication (or DB) and the client there's the API. Independent Module from the whole application.  
The API returns Data, formatted in either jSON or XML.  
  
  
## Components of an API  
  
* endpoints, or routes that will give access to input/output of data, example `web.com/movies`
* web protocol => HTTP
* response codes (200,400,500)
  
  
### Response codes  
  
* 2XX => success
* 3XX => redirection
* 4XX => error in client
* 5XX => server error
  
  
## Location of the API definition in Laravel   
  
`routes/api.php`. Example of a route accessing the movies `web.com/api/movies`, now you're loading the api.php file.  
  
  
## Types of API  
  
* REST
* GraphQL
* gRPC
  
We'll review REST and GraphQL in this course.  
  
(Link to theory)[http://wikifp.org/ca/informatica/daw/m7/uf3/api]  
  
  
## REST in Laravel  
  
Instead of Controllers we'll use `resources` and `resource collections`. It's a class designed to return **jSONs**.  
No Controller, no View. But Model stays.  
  
Resurce: retursn an object. Resource Collection returns a collection of objects (f.e. movies).  
It is similar to what a Model would be.  
Resource is pointing to Model. In the **resource** file we format thje **jSON** that we'll be returning from the **resource**.  

  
### Creating a resource  
  
`php artisan make:resource User`  
  
  
## Regarding Postman  
  
`body` => `form-data` => write request  
  
  
# Exercise  
  
Resources:
(tutorial)[http://wikifp.org/ca/informatica/daw/m7/uf2/tutorial-laravel-8/api-rest]  
(repo)[https://github.com/dantriano/m7-laravel8-tutorial/tree/api]  
  
## Exercise documentation  
  
```
/*
* Creamos los Resources y Collections de User y Products
*/

php artisan make:resource UserResource
php artisan make:resource ProductResource
php artisan make:resource UserCollection
php artisan make:resource ProductCollection
```
  
## Exercise documentation  
  
(api tut)[http://wikifp.org/ca/informatica/daw/m7/uf2/tutorial-laravel-8/api-rest]  

