# GraphQL exercises  
  
## Get user by id  
```
{
  user(id: "1") {
    id,
    name,
    email    
  }
}
```


## Get user by email  
```
{
  user(email: "dbogisich@example.com") {
    id,
    name,
    email    
  }
}
```
  
  
## Get users collection where name like String  
```
## Query with like and paginate
extend type Query {find

    users(
      name: String @where(operator: "like")
    ): [User!]! @paginate(defaultCount: 10)

}
## Request
{
  users(name: "%o%") {
    # data is a field on UserPaginator
      data {
        id
        name
        email
      }
  }
}
```

### Reformat:  
  
`schema.graphql`  
```
#import user.graphql
```
  
  
## user.graphql  
  
```
type User {
  id: ID!
  name: String!
  email: String!
}

extend type Query {
  user(id: ID! @eq): User @find
  users: [User!]! @all
}

extend type Mutation {
  createUser(name: String!, email: String!, password: String!): User @create
}
```
  
### Example queries  
  
```
### createUser request example
###### Note: can't duplicate user (probably mail field)
  mutation newUser
  {
    createUser
    (
      name: "Pakirrin",
      email: "a@c.com",
      password: "123"
    ) 
    {
      name
    }
 }
 

### user getters request example
{
  one:user(id: 1) {
    id
    name
    email
  }
  two:user(id: 2) {
    id
    name
    email
  }
  users {
    id
    name
    email
  }
}

```

## Exercises  
  
* Listar todos los productos
* Listar un producto por ID
* Crear un nuevo producto
* Modificiar un product
* Eliminar un producto
* Crear un nuevo usuario
* Cambiar los datos de un usuario
* Eliminar un usuario
* Modifica la función de listar todos productos para que ademas muestre el nombre y la id de la categoria en la misma petición
  
  
### List all products  
  
```
### Request
{
  products {
    id
    name
    category{
      id
      name
    }
  }
}
#### Response
{
  "data": {
    "products": [
      {
        "id": "1",
        "name": "Dr. Willa Ondricka Jr.",
        "category": {
          "id": "1",
          "name": "Miss Kiara Bergstrom"
        }
      },
      {
        "id": "2",
  (...)
```
  
  
### Get product by ID  
  
```
### Request
{
  product(id:3) {
    id
    name
    category{
      id
      name
    }
  }
}
#### Response
{
  "data": {
    "product": {
      "id": "3",
      "name": "Miss Kathleen Reinger IV",
      "category": {
        "id": "2",
        "name": "Ms. Nakia Pouros Jr."
      }
    }
  }
}
```
  
   
### Create a new product  
  
```
### Request
mutation newProduct
  {
    createProduct
    (
      name: "Bamboo Blind",
      desc: "Bamboo blind in 3 colors",
      price: "5.55",
      category_id: 3
      
    ) 
    {
      name
      desc
      category {
        id
        name
      }
    }
 }
#### Response
{
  "data": {
    "createProduct": {
      "name": "Bamboo Blind",
      "desc": "Bamboo blind in 3 colors",
      "category": {
        "id": "3",
        "name": "Mallie Murray"
      }
    }
  }
}
```
  
  
### Modify a product  
  
```
### Request
mutation modifyProd
  {
    modifyProduct
    (
      id: 1,
      name: "Livingroom Table",
      desc: "Wooden table, brown"
    ) 
    {
      name
      desc
      category {
        id
        name
      }
    }
 }
 #### Response
 {
  "data": {
    "modifyProduct": {
      "name": "Livingroom Table",
      "desc": "Wooden table, brown",
      "category": {
        "id": "1",
        "name": "Miss Kiara Bergstrom"
      }
    }
  }
}
```
  
  
### Delete product  
  
```
### Request
mutation modifyProd
  {
    deleteProduct
    (
      id: 2
    ) 
    {
      name
      desc
      category {
        id
        name
      }
    }
 }
 #### Response
 {
  "data": {
    "deleteProduct": {
      "name": "Josiane Mante MD",
      "desc": "Laborum error error eum voluptas sit. Odit labore quia et et qui necessitatibus. Aut dignissimos qui consequatur et ipsa.",
      "category": {
        "id": "1",
        "name": "Miss Kiara Bergstrom"
      }
    }
  }
}
```
  
  
### Create new user  
  
```
### Request
mutation myCreateuser
  {
    createUser
    (
      name: "Manolin Manolete",
      email: "manolin@manolako.com", 
      password: "1234"
    ) 
    {
      id
      name
      email
    }
 }
#### Response
{
  "data": {
    "createUser": {
      "id": "13",
      "name": "Manolin Manolete",
      "email": "manolin@manolako.com"
    }
  }
}

##################
#####  NOTE  #####
##################
Can't query the password param (in the response) since it isn't declared in the Type declaration
```
  
  
### Modify user
  
```
### Request
mutation myModuser
  {
    modifyUser
    (
      id: 5,
      name: "Juanfran Perez",
      email: "juan@fran.com", 
      password: "4567"
    ) 
    {
      id
      name
      email
    }
 }
#### Response
{
  "data": {
    "modifyUser": {
      "id": "5",
      "name": "Juanfran Perez",
      "email": "juan@fran.com"
    }
  }
}
```
  
  
### Delete user
  
```
### Request
mutation myModuser
  {
    deleteUser
    (
      id: 4
    ) 
    {
      id
      name
      email
    }
 }
#### Response
{
  "data": {
    "deleteUser": {
      "id": "4",
      "name": "Dr. Darion Lockman",
      "email": "mekhi71@example.net"
    }
  }
}
```
  
  
### Modify list all products function so they show name and ID of the category in th esame request 

```
### Request
{
  products {
    id
    name
    category{
      id
      name
    }
  }
}
#### Response
{
  "data": {
    "products": [
      {
        "id": "1",
        "name": "Livingroom Table",
        "category": {
          "id": "1",
          "name": "Miss Kiara Bergstrom"
        }
      },
      (...)
```


