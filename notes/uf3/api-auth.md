```
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Movie;
use App\Http\Resources\MovieResource;
use App\Http\Resources\MovieCollection;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


///////////////////
//// movies   /////
///////////////////

/**
 * POST movie
 * Creates a new movie
 * Checks if a movie with the same title already exists
 * returns the movie if everything is OK
 * returns Error with 400 code
 */
Route::post('/movie', function (Request $request) {
    $users = new MovieCollection(Movie::all());
    foreach ($movies as $movie) {
        if($movie->title == $request->title)
            return response()->json(['Status'=> "Error: title already in the DB, must be UNIQUE!"], 400);
    }
    return new MovieResource(Movie::create($request->all()));
});

/**
 * Get all movies
 */
Route::get('/movie', function () {
    return new MovieCollection(Movie::all());
});

/**
 * Get movies by ID
 * format: movie/id
 */
Route::get('/movie/{id}', function ($id) {
    try {
        return new MovieResource(Movie::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }

});

/**
 * Update movie (whole)
 * format: movie/id
 */
Route::put('/movie/{id}', function(Request $request, $id) {
    try {
        Movie::findOrFail($id)->update($request->all());
        return new MovieResource(Movie::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }
});


/**
 * Patch movie (partial update)
 * format: movie/id
 */
Route::patch('/movie/{id}', function(Request $request, $id) {
    try {
        Movie::findOrFail($id)->update($request->all());
        return new MovieResource(Movie::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }

});


/**
 * Delete movie (partial update)
 * format: movie/id
 */
Route::delete('/movie/{id}', function($id) {
    try
    {
        $product = Movie::findOrFail($id);
    }
    catch(Illuminate\Database\Eloquent\ModelNotFoundException)
    {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }
    $product->delete();
    return response()->json(['Status'=> 'Success!'], 200);
});


///////////////////
//// users   /////
///////////////////
/**
 * POST/register user
 * Creates a new user
 * Checks if a user with the same email already exists
 * returns the movie if everything is OK
 * returns Error with 400 code
 */
Route::post('/user', function (Request $request) {
    $users = new UserCollection(User::all());
    foreach ($movies as $movie) {
        if($movie->email == $request->email)
            return response()->json(['Status'=> "Error: email already in the DB, must be UNIQUE!"], 400);
    }
    return new UserResource(User::create($request->all()));
});

/**
 * Get all users
 */
Route::get('/user', function () {
    return new UserCollection(User::all());
});

/**
 * Get user by ID
 * format: user/id
 */
Route::get('/user/{id}', function ($id) {
    try {
        return new UserResource(User::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }

});

/**
 * Update user (whole)
 * format: user/id
 */
Route::put('/user/{id}', function(Request $request, $id) {
    try {
        Movie::findOrFail($id)->update($request->all());
        return new UserResource(User::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }
});


/**
 * Patch user (partial update)
 * format: user/id
 */
Route::patch('/user/{id}', function(Request $request, $id) {
    try {
        Movie::findOrFail($id)->update($request->all());
        return new UserResource(User::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status'=> "Error: movie doesn't exist!"], 404);
    }

});


/**
 * Delete user (partial update)
 * format: user/id
 */
Route::delete('/user/{id}', function($id) {
    try
    {
        $product = User::findOrFail($id);
    }
    catch(Illuminate\Database\Eloquent\ModelNotFoundException)
    {
        return response()->json(['Status'=> "Error: user doesn't exist!"], 404);
    }
    $product->delete();
    return response()->json(['Status'=> 'Success!'], 200);
});
```


