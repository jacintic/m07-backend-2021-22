# GraphQL  
  
When our API is finished/in production, and after that we make changes, we make a second verssion of the API.  
  
It's an abstraction of the DB.  
There's only a single endpoint, `http://localhost/graphql`  
It sends recieves **jSON**s.  
  
Hypotetical case, the client only wants the name of the clients or a specific set of attributes. With **GraphQL** you can customize your request.  
Also reducing the ammount of data transmission.  
In amazon you pay for the ammount of data transfered, optimizing the ammount of data that is sent and recieved is a way to save money.  
  
**GraphQL** is not a software it's more like a philosophy. It's back end agnostic.  
  
(Link to Teacher's docummentation on GraphQL)[http://wikifp.org/ca/informatica/daw/m7/uf3/graphql]  
 
GraphQL is very strict, and the error messages are not very intuitive.   
   
## To make it work  
  
* define type of **data** (in the **jSON**-like type format)
* define the **Queries**
  
This symbol `!` means it is required.  
  
The query part defines what your **API** will be handling/answering/returning.  
  
  
## Pseudo models in GraphQL  
  
```
@belongsToMany 

@where(operator: "like")(...) 

@eq

@all
```
 
  
## Installing **GraphQL** in Laravel  
  
```
composer require nuwave/lighthouse

php artisan vendor:publish --tag=lighthouse-schema

## Postman equivalent for GraphQL  
composer require mll-lab/laravel-graphql-playground
  
### accessing playground
http://localhost/graphql-playground

```
  
  
### Mutation  
  
Modify information.  
Defines the **interactions** that will be allowed within your **API**.  
For example, `create, update, delete`.  
  
http://localhost/graphql-playground
http://localhost/graphql-playground


## Link to tut  

  
(here)[http://wikifp.org/ca/informatica/daw/m7/uf2/tutorial-laravel-8/api-graphql]  
  
## Link to lighthouse directives  
  
(lighthouse directives)[https://lighthouse-php.com/5/api-reference/directives.html]  

