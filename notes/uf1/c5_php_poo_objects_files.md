# Files exercise  
  
## Accepted formats  
. gif
. jpg/jpeg
. png
  
## Deny list  
Not used, just accepted formats or throw error  
  
## On upload  
use:
. upload picture  
. redirect + GET parameters (success/error)
. write to .txt (pseudo DB)
   
## On success  
index.php?upload=success => tot correcte  
index.php?upload=error  
  
## Gallery  
Reads from a .txt that stores filename and related text.  
  
## Others  
Include `_header/_footer.php`  
  
## OOP  
Repasar constructores en PHP.  
  
## Class  
Create `Class` directory and store all useful classes there .  
  
## Move permissions  
```
// get user from php/nginx
$usr = exec('whoami', $output, $retval);
var_dump($usr); // it's www-data
// set owner of the directory to PHP/nginx
chown www-data:www-data upload
//after that give permissions to the directory
chmod 775 upload // group www-data can read and write
```
  
## Class exericise #2  


