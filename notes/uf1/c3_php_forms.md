# Sending data  
## GET and POST  
Two ways to send data, POST allows more characters to be sent.  
  
Those two ways are used in web forms.  
```
<form action="destination.php" method="GET(or POST)">
```
  
### Retrieving data from destination page  
It is done using `$_GET['a']` and `$_POST['a']`.  
  
### GET  
The data sent from the form will be vissible in the URL of the destination (`action`) page.  
An example of data sent by GET.  
```
detination.php?ai=11&b=3(...)
```
### FORMS  
You don't need to use a counter to fill in a form array. Example.  
```
<input type="text" name="prod[]">
```
