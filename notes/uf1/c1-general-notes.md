# Ejemplo PHP  
  
```
for (i = 0; i < 10; i++) {
	<tr>Hola</tr>
}
```
  
# Tipos de programador web  
  
. front end (presentacíon)
. back end (capa negocio y datos)
. full stack (los dos anteriores)
  
# Tipos de servidor  
. Servidor web: compila y entrega la presentación
. Servidor de apliaciones: conjunto de servicios gestionados mediante una API
  
# PHP  
. por defecto tu OS necesita un compilador de PHP, por ejemplo, LAMP
  
# Puertos  
. web => 80
. PHP =>   
  
# Docker PHP  
. PHP (compilador)
. Nginx (server)
  
localhost:1080 => enginx => compilador php archivo => enginx => navegador  
  
# CODIGOS  
500 => algo ha pasao, no se ha podido compilar la petición  
404 => Not found  
403 => forbidden  
  
# Docker  
. no puede haber mas de 1 container corriendo con el mismo nombre
. no puede haber mas de 1 container corriendo por el mismo puerto
 ^  
/!\  
___  
En el container de nginx, /app es equivalente (mediante la configuración del .env y composer.yml) a nuestro fichero /app. Estan enlazados.  
  
```
docker-compose down
docker-compose up -d
docker-compose restart
docker system prune -a
```
  



  


