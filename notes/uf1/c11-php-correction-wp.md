# PHP correction  
  
## Includes  
! usar `include_once('_footer.php')`  
  
## GET vs POST  
Get:  
. parametros a traves de URL
. no puede enviar archivos
. tiene un limite de archivos que puede enviar
  
## Otros (parametros y constantes referentes al formulario)  
Usar funciones parametrizadas que obtengan el nombre (p.e. `$_POST[PICTURE_TITLE]`) de una constante.  
Y la función es agnostica en cuanto a parametros del formulario de origen.  
  
## Otros (root del documento para rutas relativas)  
`$_SERVER['DOCUMENT_ROOT']`  
Esto te apunta a la raiz, para poder descrivir rutas absolutas y evitar `../../(...)`  
---  
`$info = getimagesize($path);`  
`$image_type = $info[2];`
`in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))`  
Esto comprueva que el tema es una imagen.  

