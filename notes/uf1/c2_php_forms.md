# Practica 1, forms  
Usar bootstrap en todas las páginas PHP.  
  
**Atención** Tener en cuenta la estructura de carpetas definida en: `https://wiki.dantriano.com/daw/m7/php`  
**Atención** Link a `http://localhost:1080`=> `index.php`.  Link a `http://localhost:1080/users/lista.php`  
  
**Atención** Seguir estructura de ficheros:  
```
APP
  => index.php
  => lista.php
  Users
    => lista.php
```

## 1.1. num productos  
Enviar numero de productos A => 
  
## 1.2. Info productos
Recive el numero de productos, construye un form con nombre y precio a rellenar.  
Envia nombre prod y precio A =>  
  
## 1.3. Tabla de muestra de productos con su info.  
Recive la info de los productos y construye una tabla con Bootstrap.  
**ATENCIÓN** Si un producto no tiene datos tendra que ser rempazado con un placeholder.  
  
 
  

