# Laravel  
  
## Content  
[deploying](#user-content-deploying)  
[migrations and seeding](#user-content-db-migrations-and-seeding)  
[mix](#user-content-mix)  
[routes](#user-content-routes)  
[controllers](#user-content-controllers)  
[models](#user-content-models)  
[blade](#user-content-blade)  
[layouts](#user-content-layouts)  
[factories](#user-content-factories)  
  
  
## Deploying
  
[sail](https://laravel.com/docs/8.x/sail)  
```

# first pull of the project?
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs


# project is ready? launch
./vendor/bin/sail up

```
[▲](#user-content-content)  

  
## DB migrations and seeding  
   
[migrate comands](https://laravel.com/docs/8.x/eloquent)
```
# creates migratio seeder and model
./vendor/bin/sail php artisan make:model Testo -ms
```
  
[example migration (movies)](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/database/migrations/2021_11_29_171713_create_movies_table.php)  
[example migration (moviesxgenere)](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/database/migrations/2021_11_29_172921_create_movies_x_genres_table.php)  

[migrate](https://laravel.com/docs/8.x/migrations)
```
php artisan migrate:fresh --seed
``` 
[▲](#user-content-content)  

  
## Mix  
  
[mix](https://laravel.com/docs/8.x/mix)  
[js files](https://gitlab.com/iawKeilaGonzalez/petflix/-/tree/main/resources/js)  
[css files](https://gitlab.com/iawKeilaGonzalez/petflix/-/tree/main/resources/css) 
[▲](#user-content-content)   
```
mix.js('resources/js/app.js', 'public/js')
    .css('resources/css/app.css', 'public/css');

./vendor/bin/sail npm install
./vendor/bin/sail npm run dev
``` 
[▲](#user-content-content)  
  

## Routes  
  
[routes](https://laravel.com/docs/8.x/routing)  
[my routes](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/routes/web.php)  
[▲](#user-content-content)
```
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;


Route::get('/', function () {
    return view("prehome");
});


Route::post('/home', [HomeController::class, 'getHomeMovies']);
Route::get('/home', [HomeController::class, 'getHomeMovies']);

# from view
<form action="/home" method="post">
  @csrf
  (...)
  <button class="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit">START MEMBERSHIP</button>
</form>

# from controller
public function getHomeMovies(Request $request) 
{
  $generes = ['action','comedy'];
  $collection = [];
  foreach ($generes as $genere) {
    $collection[] = $this->getGenere($genere)->get();
  }
  return view('home')->with('movieCollection', $collection);
}

```
[▲](#user-content-content)  
  
  
## Controllers  
  
[laravel controllers](https://laravel.com/docs/8.x/controllers)
[my controllers](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/app/Http/Controllers/HomeController.php)  
[▲](#user-content-content)  
```
## Make controller command
php artisan make:controller TestController

## Physical controller
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /*
    public function getHomeMovies()
    {
        $movie = new Movie();
        $movies = $movie::all();
        return view('home')->with('action', $movies);
    }
    */

    public function getHomeMovies(Request $request) 
    {
        $generes = ['action','comedy'];
        $collection = [];
        foreach ($generes as $genere) {
            $collection[] = $this->getGenere($genere)->get();
        }
        return view('home')->with('movieCollection', $collection);
    }

    public function getGenere($genere)
    {
        $movie = Movie::query();
        $movies = $movie->myjoin();
        $movies = $movie->genere($genere);
        return $movies;
    }
}

```
[▲](#user-content-content)  
  

## Models  
  
[Laravel Models](https://laravel.com/docs/8.x/eloquent-serialization#serializing-models-and-collections)  
[My Models](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/app/Models/Movie.php)  
[▲](#user-content-content)  
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    public function scopeGenereWhole($query)
    {
        return $query->orderBy('title')
            ->take(10);
	}

    public function scopeWhereParamEq($query, $param, $paramEq)
    {
        return $query->where($param, $paramEq);
	}

    public function scopeOrderby($query, $param)
    {
        return $query->orderBy($param);
	}

    public function scopeLimit($query, $n)
    {
        return $query->take($n);
	}
    public function scopeMyjoin($query)
    {
        return $query->join('movies_x_genres', 'movies.id', 'id_movie')
            ->join('genres', 'id_genre', 'genres.id')
            ->select('movies.*', 'name as gname');
	}
    public function scopeGenere($query, $genre)
    {
        return $query->where('name',$genre);
	}
}

```
[▲](#user-content-content)  
  

## Blade
  
[blade laravel](https://laravel.com/docs/8.x/blade)  
[my forelse](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/resources/views/home.blade.php)  
[▲](#user-content-content)  
```
# forelse
@forelse ($users as $user)
    <li>{{ $user->name }}</li>
@empty
    <p>No users</p>
@endforelse

#ifelse
@if (count($records) === 1)
    I have one record!
@elseif (count($records) > 1)
    I have multiple records!
@else
    I don't have any records!
@endif

# simple if
@if ($user->type == 1)
    (...)    
@endif

```
[▲](#user-content-content)  
  

## Layouts  
  
[laravel layours](https://laravel.com/docs/8.x/blade#building-layouts)  
[my layouts](https://gitlab.com/iawKeilaGonzalez/petflix/-/tree/main/resources/views)  
[▲](#user-content-content)  
```
# layout
(...)
@yield('content')
(...)

# view
@extends('layouts.main')
@section('content')
(...)
@endsection
```
[▲](#user-content-content)  
  

## Factories  
  
[laravel factories](https://laravel.com/docs/8.x/database-testing#defining-model-factories)  
[my factories file](https://gitlab.com/jacintic/petflix-testing/-/blob/factory/database/factories/MovieFactory.php)  
[seeder modified for factory, bottom](https://gitlab.com/jacintic/petflix-testing/-/blob/factory/database/seeders/MovieSeeder.php)  
[factories apuntes Santi](https://gitlab.com/iawKeilaGonzalez/petflix/-/blob/main/document/documentacion_migraciones/documentacion_migraciones.md)
[▲](#user-content-content)  
```
# create a factory

./vendor/bin/sail php artisan make:factory MovieFactory --model=Movie


# check details to add to your factory in your own factory file


# tinker 

./vendor/bin/sail php artisan tinker


# inside tinker

\App\Models\Movie::factory()->create();
## create n rows
\App\Models\Movie::factory()-count(9)->create();


# inside seeder

\App\Models\Movie::factory(3)->create();




```
[▲](#user-content-content) 
