# Laravel  
  
  
## Aplicaciones web  
. Parte servidor
. Parte cliente  
Laravel es parte cliente.  
  
Laravel sigue el patron de diseño de Apps MVC.  
Se dividen los componentes/peticiones en 3 grandes familias:  
. vista (html/css/con datos dinamicos PHP)
. controlador => reciven la petición (por egemplo URL), decide la vista y la data que recive la vista. Manipulación de datos va en esta capa.
. modelo trata con los datos puros, por ejemplo SQL, queries, bases de datos
  
  
## Gestores de paquetes (en Laravel)  
  
composer.json  
Ejemplo de paquete en el composer.json:  
. Nombre
. Version
. Dependencias
. Url Repositorio
  
Ejemplo de instalación de paquete:  
```
composer require auth0/login:"~6.0"
```
**ATENCIÓN** en el mismo directorio de la raiz del proyecto (donde estara tb el **composer.json**).  
**ATENCIÓN** todos estos paquetes estaran instalados en un directorio (p.e. /vendor), por defecto Laravel tendra en el **.gitignore** ignorados los paquetes instalados pro **composer.json**.  
  
  
## routes  
`routes/web.php`  
```
// en este caso se salta el controller y directamente va a la vista
Route::get('/', function () {
	return view('welcome');
})->name('home');

  
// este router llama a un controlador
Route::get('/dashboard', [
	'uses' => 'UserController@getDashboard',
	'as' => 'dashboard'
]);
  
// post
Route::post('/signup0, [
	'users' => 'UserCOntroller@postSignup',
	'as' => 'signup'
]);
```  
  
  
## Controller  
Se usa un controlador por tipo de bloque abstracto, por ejemplo, usuarios.  
Otro ejemplo, tienda => controladorProductos  
. filtrar prod
. añadir prod
. eliminar prod
Compra  
. añadir a carrito
. checkout
. returar del carrito
. etc.
  
Pelis controller  
. editar peli
. ver peli
. listar peli
. añadir peli
  
### Request  
Información hacia el controller que proviene de un formulario.  
  
  
### list(productListRequest $request) {
```
	$request->flash();

	$productos = $this->producti->query();

if ($request->filled('priceMin')) {
	$products->priceMin($request->input('prioceMin');
}
(...) // multiples requests como esa

return view(('products)->with('productos', $products->get();
```
  
#### Conceptualización peliculas favoritas de usuario.  
El usuario, tiene peliculas favoritas. Por lo tanto el usuario tiene una lista con IDs de peliculas que son sus favoritas.  
  
  
## Modelo  
```
... scopePriceMin ($query, $input) {
    return $query->where('price,'>', $input);
} 
```
`app/Models/[modelo].php`
El nombre de la Class le indica al Modelo a la tabla a la que acceder.  
```
class Product exstendds Model {(...)}
```  
Se puede en el modelo especificar el nombre de la tabla si se sale del patron Product => products(tabla).
**Una tabla un modelo**  
  

## Migrations  
se ejecuta en el `sail`.  
`->index();` -> optimización de campo para filtro de datos por campo (ordenar
Algunos metodos:  
. up => crea tabla (tiene atributos)
. down => drop table (borra tabla)  
  
Versiones de "BBDD/Tablas"  
V1 -> products
V2 -> products + IVA

### Eloquent motor de BBDD de Laravel => Modelo
  
    
# Laravel 8  
`docker composer up -d` y ya funciona todo.  
. db => mySQL  
. sail => php

## package.json (de node)  
  
Es un gestor de paquetes per para Node.  
  
  

