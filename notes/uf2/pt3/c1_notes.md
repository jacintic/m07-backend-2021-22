# Laravel notes  
  
## 4 things  
. requests
. sessions
. authentication
. middlewares  
  
  
## Form validation  
Use FormRequest  
  
  
## Sessions  
Used for auth/login, keep data from user AFTER login so he'll be AUTHORIZED to browse his private/authenticated content.  
  
Session is an object with key value pairs where you'll store necessary info to provide browsing experience to a logged in user.  
  
### Cookies  
Same as sessions but within the browser.  
  
Less secure as they can be easily accessed and modified by the user.  
  
A simple script can view cookies of all the sites you've browsed in the last 5 years. For example.  
  
### Sessions how do they work  
```  
$request->session()->get('carrito',[]) // get session obj
$request->session()->put('carrito',$carrito) // write into session obj
$request->session()->forget('carrito'); // delete/clear session obj
```
  
  
## Authentication
Session Guard  
Login controller  
It is interconected with **Model**, **Controller**, **Routes** and **Blade**.  

Installation of Guard component.    
`composer require laravel/breeze --dev`  
  

This order provides already made and working views, Controllers, etc.  
```
php artisan breeze:install

npm install
npm run dev
php artisan migrate
```

## Middlewares  
For example, if user == admin, return true (the middleware continues it's route to the controller when it returns true.  
  
Abstracting components, like auth away from the controller.  
  
Another example  
Logging all the user's actions.  
Instead of including it on each and every controller, create a middleware that does this and call it along the controllers in the routes.  

# Documentation
[codez](http://wikifp.org/ca/informatica/daw/m7/uf2/tutorial-laravel-8/)  

