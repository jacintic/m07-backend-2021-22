# Laravel  
  
## Content  
[requests](#user-content-requests)   
[redirects](#user-contet-redirect)   
[middleware](#user-contet-middleware)   
  
  
## Requests
  
[sail](https://laravel.com/docs/8.x/sail)  
[requests Laradox](https://laravel.com/docs/8.x/validation#form-request-validation)  
[requests LaraApi](https://laravel.com/api/8.x/Illuminate/Foundation/Http/FormRequest.html)  
[▲](#user-content-content)   
```
# artisan command
php artisan make:request HomePostRequest


# in controller
## on top
use App\Http\Requests\SearchFormRequest;
## rest
public function searchHome(HomePostRequest $request)
{

    //dd($request->flick);
}
  

# in request
public function rules()
{
return [
    'flick' => 'required|max:255'
];
}

/**
* Get the error messages for the defined validation rules.
*
* @return array
*/
public function messages()
{
return [
    'flick.required' => "A flick term is required to search",
    'flick.max' => "Flick term too long, can't exceed 255 characters",
];
}


# in route
Route::post('/search', [HomeController::class, 'searchHome'])->name('searchVids');


# in form
<form method="POST" action={{ route('searchVids') }}
    class="d-flex
        align-items-center
        justify-items-center">
    @csrf
    <input name="flick" class="form-control me-2" type="search" placeholder="Search"
        aria-label="Search">
    <!-- search icon -->
    <button type="submit" class="btn text-light">
        <i class="bi-search"></i>
    </button>
</form>
@error('flick')
    <div class="alert alert-danger" role="alert">
        {{ $message }}
    </div>
@enderror



```
[▲](#user-content-content)  


## Redirect
[Laravel redirects](https://laravel.com/docs/8.x/redirects)
[▲](#user-content-content)   
```
# return a previo pero con el Old funcionando
return redirect()->back()->withInput();
```
[▲](#user-content-content)   
  
  
## Middleware  
[Laravel middlewares](https://laravel.com/docs/8.x/middleware)  
[▲](#user-content-content)   
```
## create one
php artisan make:middleware EnsureTokenIsValid


## alias
app/Http/Kernel.php ## file with the alias

protected $routeMiddleware = [
    'auth' => \App\Http\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
    'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
    'can' => \Illuminate\Auth\Middleware\Authorize::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
    'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
];
## array with the aliases


## routes
Route::get('/profile', function () {
    //
})->middleware('auth');

```
[▲](#user-content-content)   
