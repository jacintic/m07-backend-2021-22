# Node  
  
  
## npm  
  
Package manager.  
  
  
## Express  
  
A node framework.  
  
  
## Nodemon  
  
A development package that acts as a daemon for node to watch for changes live.  
  
  
## First steps  
  
The **DB** of choice will be **MongoDB**.  
The project's sturcture will pretty much be the same as Laravel's, following the **MVC** pattern.  
The project will be creating a **chat**, using **sockets**.  
The main drifferences between **sockets** and **async** features like **ajax** or **async/await** are:
* sockets listens to server continuously
* sockets require more resources from the server
  
  
### Installation  
  
(link to wiki)[https://wikifp.org/ca/informatica/daw/m7/uf4/Tutorial-Node/instalacio-configuracio]  
  
Instructions: 
```
apt install npm
mkdir node-tutorial
npm init
## entry point: app.js

npm install express --save
```
  
  
### First program
  
* app.js
```
var express = require('express');

var app = express();
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
```
  
  
### Launching first program  
  
```
# in root folder
node app.js

## now you can visit on localhost:3000/
```
  
  
### Introducing dev enviroment  
  
```
## in package.json

## adding dev dependencies
"dependencies": {....},
"devDependencies": {
    "nodemon": "^1.18.10"
}

## npm install

## adding the test scripts/nodemon
"scripts": {
    "test": "nodemon app.js",
    "start": "node app.js"
},

## npm run test

```
