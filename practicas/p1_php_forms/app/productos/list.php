<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>

<body>
    <div class="row">
        <div class="col-md">
            <div class="p-3 mb-3 bg-dark text-white">Enter products data</div>
        </div>
    </div>
    <!-- GET POST DATA AND ASSIGN TO VARIABLE -->
    <?php
    $total_products = $_POST['num_products'];
    if ($total_products <= 0) {
    ?>
        <p>Sorry, there are no products.</p>
    <?php
    }
    ?>
    <div class="row">
        <div class="col-md">
            <form action="./products/products.php" method="POST">
                <?php
                if ($total_products > 0) {
                    for ($i = 0; $i < $total_products; $i++) {

                ?>
                        <!-- iterate through counter from POST numbers -->


                        <div class="row mb-3 justify-content-md-center">
                            <label for="nameProd" class="col-sm-2 col-form-label text-end">Product Name</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="nameProd" name="prod[ <?php echo $i ?> ][name]">
                            </div>
                            <label for="prodPrice" class="col-sm-1 col-form-label text-end">Price</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="prodPrice" name="prod[ <?php echo $i ?> ][price]">
                            </div>
                        </div>
                    <?php

                    }

                    ?>
                    <!-- END PODUCT FORM STATIC  -->
                    <div class="row mb-3 justify-content-md-center">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
<?php
                }
?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>