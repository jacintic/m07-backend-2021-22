<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>

<body>
    <div class="row">
        <div class="col-md">
            <div class="p-3 mb-3 bg-primary text-white">Products data</div>
        </div>
    </div>
    <?php
    $prods = $_POST['prod'];
    $total_prods;
    // error control for page loading without recieving 'prod' parameter from its previous page (i.e. just accessing /products/products.php)
    if (isset($prods)) {
        $total_prods = count($prods);
    }
    // error control for 0 products being displayed
    if ($total_prods <= 0) {
    ?>
        <p>There are no products</p>
    <?php } ?>
    <?php
    // contdition if there are products, format tables
    if ($total_prods > 0) {
        $counter = 0;
    ?>
        <div class="row justify-content-center">
            <div class="col-9">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr class="bg-dark text-light">
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($prods as $prod) {
                            $counter++;
                        ?>
                            <tr>
                                <?php
                                if ($prod['name'] == '' and $prod['price'] == '') {
                                ?>
                                    <th scope="row">Product not inserted</th>
                                    <td></td>
                                    <td></td>
                                <?php
                                } else {
                                ?>
                                    <th scope="row"> <?php echo $counter ?> </th>
                                    <td> <?php echo $prod['name'] ?></td>
                                    <td><?php echo  $prod['price'] ?></td>
                                <?php
                                }
                                ?>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>