<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Football App</title>
</head>

<body>
    <!-- FORM + BOOTSTRAP HTML -->
    <div class="row">
        <div class="col-md">
            <div class="p-3 mb-3 bg-dark text-white">Football App</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <form action="./futbol_02.php" method="POST">
                <div class="row mb-3 justify-content-md-center">
                    <label for="players" class="col-sm-2 col-form-label text-end">Number of players</label>
                    <div class="col-sm-2">
                        <input type="number" class="form-control" id="players" name="players">
                    </div>
                    <label for="matches" class="col-sm-2 col-form-label text-end">Number of matches</label>
                    <div class="col-sm-2">
                        <input type="number" class="form-control" id="matches" name="matches">
                    </div>
                    <div class="col-sm-1">
                        <input type="submit" class="form-control" value="Send">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>