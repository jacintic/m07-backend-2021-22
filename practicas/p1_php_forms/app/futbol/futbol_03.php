<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Football App</title>
</head>

<body>
    <div class="row">
        <div class="col-md">
            <div class="p-3 mb-3 bg-dark text-white">Football App</div>
        </div>
    </div>
    <?php
    // get post data and asign it to variables
    $player = $_POST['player'];
    if (!isset($player) or count($player) <= 0) {
    ?>
        <p>Sorry, there are not enough players or matches.</p>
    <?php
    }
    // condition there are players or matches
    if (count($player) > 0) {
    ?>
        <div class="row justify-content-center">
            <div class="col-9">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr class="bg-dark text-light">
                            <th scope="col">Player</th>
                            <?php
                            // print header of the table, iterate through matches/goals
                            for ($m = 0; $m < count($player[0]["goals"]); $m++) {
                            ?>
                                <th scope="col">Match <?php echo ($m + 1); ?> </th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // iterate through matches/players
                        foreach ($player as &$pl) {
                        ?>
                            <tr>
                                <?php
                                // get name
                                ?>
                                <th scope="row"> <?php echo $pl['name'] ?> </th>
                                <?php
                                // get goals by match and player
                                for ($i = 0; $i < count($pl['goals']); $i++) {
                                ?>
                                    <td> <?php echo $pl['goals'][$i] ?></td>
                            <?php
                                }
                            }
                            ?>
                            </tr>
                        <?php
                    }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>