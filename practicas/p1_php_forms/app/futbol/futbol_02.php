<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Football App</title>
</head>

<body>
    <div class="row">
        <div class="col-md">
            <div class="p-3 mb-3 bg-dark text-white">Football App</div>
        </div>
    </div>
    <?php
    // get post data and asign it to variables
    $players = $_POST['players'];
    $matches = $_POST['matches'];
    // condition there are no players or matches
    if ($players <= 0 or $matches <= 0) {
    ?>
        <p>Sorry, there are not enough players or matches.</p>
    <?php
    }
    // condition there are players or matches
    if ($players > 0 or $matches > 0) {
    ?>
        <div class="row">
            <div class="col-md">
                <form action="./futbol_03.php" method="POST">
                    <div class="row justify-content-center">
                        <?php
                        // players loop
                        for ($i = 0; $i < $players; $i++) {
                        ?>
                            <div class="col-10 p-3 border border-info">
                                <div class="row mb-3">
                                    <label for="player" class="col-sm-2 col-form-label">Player's Name</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="player" name="player[<?php echo $i ?>][name]">
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <p class="col-sm-3">Player's Goals by match</p>
                                </div>
                                <div class="row mb-3">
                                    <?php
                                    // matches loop
                                    for ($j = 0; $j < $matches; $j++) {
                                    ?>

                                        <label for="goals-match" class="col-sm-1 col-form-label text-end">Goals Match <?php echo ($j + 1); ?></label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="goals-match" name="player[<?php echo $i ?>][goals][<?php echo $j; ?>]">
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="col-10 ps-0">
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                </form>
            </div>
        <?php
    }
        ?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>