<?php
// this will be the title of the homepage
$pg_title = 'Welcome to the Gallery!';
$pg_header = 'Upload picture';
// calling to header with title, bootstrap css and header(bootstrap div)
include_once './Includes/_header.php';
?>
<div class="row justify-content-center">
    <div class="col-10 border rounded">
        <!-- Title of the Form -->
        <h5 class="mt-3 fw-bold">Upload Picture</h3>
            <form action="upload_manager.php" method="POST" enctype="multipart/form-data">
                <!-- Title -->
                <div class="mb-3">
                    <label for="title" class="form-label">Title:</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <!-- Picture -->
                <div class="mb-3">
                    <label for="picture" class="form-label">Picture:</label>
                    <input type="file" class="form-control" id="name" name="picture">
                </div>
                <!-- Submit button -->
                <button type="submit" class="btn btn-primary mb-3">Upload</button>
            </form>
    </div>
</div>
<?php
include_once './Includes/_footer.php';
?>