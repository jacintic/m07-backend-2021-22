<?php
///////////////
// CONSTANTS //
///////////////
// path to the upload folder
define("PATH", "pictures/");
// filetypes allowed
define("FILE_ALLOW_LIST", array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png"));
// max allowed filesize (in MB)
define("MAXSIZE", 3 * 1024 * 1024);
////////////////
// Exceptions //
class UploadError extends Exception
{
}
///////////////////////
// Upload Class propper
class Upload
{
  // attributes
  private $_picture;
  private $_success;
  /*
  * Constructor: Inicia la subida del archivo
  * Entrada:
  *   $name: Nombre del elemento del formulario del que gestionamos el $_FILE
  */
  function __construct($name)
  {
    if (isset($name)) {
      $this->_picture = $name;
      $this->_success;
      $this->upload();
    }
  }

  /*
  * upload: Función que hace las operaciones necesarias para subir el archivo
  * al servidor
  */
  public function upload()
  {
    try {
      // save the filename of the picture
      $filename = $this->_picture["name"];

      // check for file within admited formats (image)
      $ext = pathinfo($filename, PATHINFO_EXTENSION);
      if (!array_key_exists($ext, FILE_ALLOW_LIST)) {
        throw new UploadError('<b>Error:</b> Unsuported file type. Only image files allowed.');
      }

      // check for file size within limits  
      if ($this->_picture['size'] > MAXSIZE) {
        throw new UploadError('<b>Error:</b> File size over max file size.' . (MAXSIZE / (1024 * 1024)) . 'MB = Max allowed file size.');
      }

      // check for filename duplicates
      if (file_exists(PATH . $filename)) {
        throw new UploadError('<b>Error:</b> File or file with same filename already exists.');
      }

      // /// ///// FINALLY UPLOAD the image
      move_uploaded_file($this->_picture["tmp_name"], PATH . $filename);

      // Set the global to be tracked by DBwriter
      $this->_success = true;

      // redirect on success
      header('Location: index.php?Status=true&Msg=Picture uploaded successfully!');

      //// CATCH STANCES WITH REDIRECT ///
    } catch (UploadError $e) {
      $this->_success = false;
      header('Location: index.php?Status=false&Msg=' . $e->getMessage());
    } catch (Exception $e) {
      $this->_success = false;
      // aqui iria el redirect al desto con parametro GET
      header('Location: index.php?Status=false&Msg=' . $e->getMessage());
    }
  }
  /*
  * Getters. Lo que quiere decir que los atributos de la clase son private
  */
  public function getPath()
  {
    return PATH . $this->_picture['name'];
  }
  public function getSuccess()
  {
    return $this->_success;
  }
  /*
  * OPCIONAL:
  * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
  * en la subida de archivos. Por ejemplo:
  * throw new UploadError("Error: Please select a valid file format.");
  */
}
