<?php
///////////////
// CONSTANTS //
///////////////
// path to the upload folder
define('PATH_DB', 'db/');
define('FILE', 'fotos.txt');
////////////////
// Exceptions //
/*
  * OPCIONAL:
  * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
  * en la subida de archivos. Por ejemplo:
  * throw new UploadError("Error: Please select a valid file format.");
  */
class FileError extends Exception
{
}
///////////////////////
// Upload Class propper
class File
{
  //////////////////////
  /// MAIN FUNCTIONS ///
  //////////////////////
  /*
  * writeToDb: Función que hace las operaciones necesarias para subir el archivo
  * al servidor
  * return: void
  */
  public function writeToDb($pictureUrl, $pictureTitle)
  {
    try {
      // write
      $file = fopen($this->getDBPath(), 'a');
      // string formatter takes care of the string inclding a "\n" at the begining or not based on
      // db being empty or not 
      fputs($file, $this->stringFormatter($pictureTitle, $pictureUrl, $this->isDBEmpty()));
      fclose($file);
      // error reporting
    } catch (Exception $e) {
      // aqui iria el redirect al desto con parametro GET
      header('Location: index.php?Status=false&Msg=<b>Error:</b> ' . $e->getMessage());
    }
  }
  
  ////////////////// 
  // UTILITIES /////
  //////////////////
  /*
  * stringFormatter: formatea la url i el titulo de la imagen de la siguiente forma:
  * Clase con alumnos###pictures/teacher.png
  * Montaña Bonita###pictures/mountain.png
  * ATENCIÓN tiene en cuenta si es la primera escriptura a la DB o no y lo hace añadiendo 
  * "\n" si no es la primera escritura y omitiendolo si es la primera, con eso se evita la linea fantasma al recorrer el objeto despues
  * return: String, titulo mas ruta a la imagen
  */
  public function stringFormatter($title, $pic, $empty)
  {
    // return "\n" + string when db is not empty to avoid ghost lines
    return (($empty) ? '' : "\n") . $title . '###' . $pic; 
  }
  /*
  * isDBEmpty: Función que derermina si la BD (fotos.txt) esta vacia o no a partir del tamaño del archivo
  * return: 
  *       - ture (db esta vacia)
  *       - false (db tiene contenido)
  */
  public function isDBEmpty()
  {
    return (filesize($this->getDBPath()) > 0) ? false: true;
  }
  /**
   * get db path: gets the path to db file
   * return: String, url to db=>fotos.txt
   * 
   */
  public function getDBPath() {
    return PATH_DB . FILE;
  }
}
