<?php
class Picture
{
    // attributes
    private $_filename;
    private $_title;
    /*Constructor*/
    function __construct($title, $fileName)
    {
        $this->_filename = $fileName;
        $this->_title = $title;
    }

    /*
  *Getters. Lo que quiere decir que los atributos de
  *title y filename son private
  */
    public function title()
    {
        return $this->_title;
    }
    public function fileName()
    {
        return $this->_filename;
    }
}
