<?php
class Gallery
{
    private $_gallery = [];
    private $_db;

  /*Constructor: Recibe la ruta del archivo fotos.txt*/
    function __construct($filename){
        $this->_db = $filename;
        $this->loadGallery();
    }

    /*
  *Recorre el archivo fotos.txt y para cada linea, crea un
  *elemento Picture que lo añade al atributo $_gallery[]
  */
    function loadGallery(){
        $file = fopen($this->_db, 'r');
        while (!feof($file)) {
            // get line and split between title and url
            $line = explode('###', fgets($file));
            // add picture to the gallery
            array_push($this->_gallery, new Picture($line[0],$line[1]));
        }
        fclose($file);
    }

    /*
  *Getters.
  * return: returns the gallery array with the Picture objects in it
  */
    public function getGallery(){
        return $this->_gallery;
    }
}
?>