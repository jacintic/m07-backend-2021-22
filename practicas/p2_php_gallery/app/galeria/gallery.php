<?php
// variables to pass to header
$pg_title = 'Welcome to the Gallery!';
$pg_header = 'Gallery';
// header
include_once('Includes/_header.php');
// Classes
include('Class/FileClass.php');
include('Class/PictureClass.php');
include('Class/GalleryClass.php');
// $file => 
// used to retrieve path to DB to avoid DB path duplications
// used to determine if DB is empty or not (to avoid prtinging elements)
$file = new File();
$gallery = new Gallery($file->getDBPath());
// print page layout
?>
<div class="row m-5">
    <?php
    // this line (if) is required to avoid ghost printing (printing empty divs)
    if (!$file->isDBEmpty()) {
        foreach ($gallery->getGallery() as $picture) {
    ?>
            <div class="mx-2" style="box-sizing:border-box;width:32%">
                <div class="row justify-content-center">
                    <div class="col border rounded" style="overflow:hidden;">
                        <div class="row mt-0 justify-content-center bg-dark align-middle" style="max-height: 30vh; min-height: 30vh;">
                            <img src='<?= $picture->fileName() ?>' style="max-height:30vh;max-width:calc( 100% + 25px);width: auto; height: min-content; margin-top: auto; margin-bottom:auto;">
                        </div>
                        <div class="row" style="margin-top:calc( 1rem + 8px);">
                            <h5>
                                <p><span class="fw-bold"><?= $picture->title() ?></p>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        <?php

        }
    }
    // ALERT: DB IS EMPTY
    if ($file->isDBEmpty()) {
        ?>
        <div class="col-8 m-3">
            <div class="alert alert-danger mt-3" role="alert">
                Gallery is Empty, no pictures here for now.
            </div>
        </div>
    <?php
    }
    ?>
</div>
<br>
<?php
include_once './Includes/_footer.php';
?>