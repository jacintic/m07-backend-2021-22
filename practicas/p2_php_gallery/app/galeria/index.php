    <?php
    // this will be the title of the homepage
    $pg_title = 'Welcome to the Gallery!';
    $pg_header = 'Gallery App';
    // calling to header with title, bootstrap css and header(bootstrap div)
    include_once './Includes/_header.php';
    ?>
    <div class="row justify-content-center">
        <div class="col-10 border rounded">
            <?php
            /////////////////////////////////
            ///// ERROR/STATUS MESSAGES /////
            /////////////////////////////////
            //                             //
            // check and display ERROR/SUCCESS messages
            // if status is 'true' then the alert class will be success
            // if status is 'false' then the alert class will be danger
            if (isset($_GET['Status'])) {
            ?>
                <div class="alert <?= (filter_var($_GET['Status'], FILTER_VALIDATE_BOOLEAN)) ? 'alert-success' : 'alert-danger'// prints type of alert (class) accordingly ?>  mt-3" role="alert">
                    <?= $_GET['Msg'] ?>
                </div>
            <?php
            }
            ?>
            <div class="card">
                <div class="card-body">
                    <div class="bd-example">
                        <a type="button" class="btn btn-primary" href="addPicture.php">Add picture</a>
                        <a type="button" class="btn btn-success" href="gallery.php">View Gallery</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include_once './Includes/_footer.php';
    ?>