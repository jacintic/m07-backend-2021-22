<?php
////////////////
// CONSTANTS //
////////////////
define("PICTURE", "picture");
////////////////
// Exceptions //
////////////////
class ManagerError extends Exception
{
}
// include upload class
include('Class/UploadClass.php');
// function that cheks for permissions and directories/files existing
function checkPermissions(){
        // check if directory exists
    if(!file_exists(PATH) or !file_exists('db/fotos.txt')) {
        header("Location: index.php?Status=false&Msg=Directory 'pictures/' and/or 'db/fotos.txt' doesn't exist.");
    // check for propper permissions within directories/files
    } elseif(
            // check permissions of upload directory
            substr(decoct(fileperms(PATH)),2) != 777
            // check permissions of pictures directory
            or
            substr(decoct(fileperms('db/')),2) != 777   
            // check permissions of db/fotos.txt file
            or
            substr(decoct(fileperms('db/fotos.txt')),2) != 777 
            ) {
        header("Location: index.php?Status=false&Msg=Directoreis/files '" . PATH . "' and/or 'db/fotos.txt' do not have propper permissions , for example 777.");
        return false;
    }
    return true;
}


// launch process to detect POST and check for empty titles or pictures
try {
    // Check if the form was submitted
    if (!$_SERVER["REQUEST_METHOD"] == "POST") {
        // Check if file was uploaded without errors
        throw new ManagerError('<b>Error:</b> POST method not detected.');
        if (!isset($_FILES["picture"]) && !$_FILES["picture"]["error"] == 0) {
            throw new ManagerError('Errors while uploading.');
        }

    }
    ////////////////////////////////////////////////
    ////// CHECK FOR EMPTY TITLE AND PICTURE ///////
    ////////////////////////////////////////////////
    $filename = $_FILES[PICTURE]['name'];
    // error control for empty title and pciture (abort uploading)
    if ($_POST['title'] == '' && $filename == '') {
    throw new ManagerError('<b>Error:</b> you must select a picture and introduce a title for the picture.');
    }
    // error control for empty title (abort uploading)
    if ($_POST['title'] == '') {
    throw new ManagerError('<b>Error:</b> you must introduce a title for the picture.');
    }
    // error control for empty picture (abort uploading)
    if ($filename == '') {
    throw new ManagerError('<b>Error:</b> you must select a picture to upload.');
    }
    ///////////////////////////////////////
    ///// INITIALIZE PICTURE UPLOAD  //////
    ///////////////////////////////////////
    // if permissions are in order proceed to upload the picture
    if(checkPermissions()) {
        // IF EVERYTHING WENT RIGHT => upload picture
        uploadPicture();
    }
}catch (ManagerError $e) {
    header('Location: index.php?Status=false&Msg=' . $e->getMessage());
}catch (Exception $e) {
    header('Location: index.php?Status=false&Msg=' . $e->getMessage());
}
/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Llamada a escritura a fotos.txt solo si se ha subido la foto correctamente
* Return: void.
*/
function uploadPicture(){
    $upload = new Upload($_FILES["picture"]);
    if ($upload->getSuccess()) { 
        // upload picture calls on DB write on success
        addPictureToFile($upload->getPath(),$_POST['title']);
    }
}

/*
* Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
* fotografía recien subida
* Entradas:
*       $file_uploaded: La ruta del archivo
*       $title_uploaded: El titulo del archivo
* Return: void
*
*EJEMPLO DE FORMATO de DB:
* Clase con alumnos###pictures/teacher.png
* Montaña Bonita###pictures/mountain.png
*/
function addPictureToFile($file_uploaded,$title_uploaded){
    include('Class/FileClass.php');
    $file = new File();
    $file->writeToDb($file_uploaded, $title_uploaded);
}

?>